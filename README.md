# access
git clone git@gitlab.com:hayainaito/test-przychodnia-ruby.git
or
git clone https://gitlab.com/hayainaito/test-przychodnia-ruby.git

# Set it to work
1. npm install -> Javascritp libraries install
2. bundle install -> Gem's install
3. rake db:migrate -> Init of DB:tables structures
4. rake medical_patients_importer:import OR rails runner lib/scripts/import_patients.rb -> Import all data of patients from csv file
5. rails db:seed

# TODO
1. Using controller, and load graph data by Ajax (from that controller)
2. Using Stimulus.js
3. Better to use modal on delete confirm (visits)
4. Better to use: https://github.com/Choices-js/Choices
5. Remove comment HTML
6. Match all files indent (now is: 2 or 4, set all to: 2)
7. Polonization of pagination
8. Merge views: visit_new, visit_edit
