namespace :medical_patients_importer do
  desc "import medical patients csv data file to patients table"
  task import: :environment do
    path = File.join(Rails.root, "db/csv/fake_medical_patients.csv")

    list = []
    CSV.foreach(path, headers: true) do |row|
      # pesel
      pesel = row["pesel"].to_s

      # gender
      gender = (pesel[6,4].to_i % 2 == 0) ? "w" : "m"

      # birthday
      ## year-month-day
      born_year = pesel[0,2].to_i
      born_month = pesel[2,2].to_i
      born_day = pesel[4,2]

      ## special month
      if born_month >= 1 and born_month <= 12 then
        born_year = "19" + born_year.to_s
        born_month = born_month.to_s
      elsif born_month >= 21 && born_month <= 32 then
        born_year = "20" + born_year.to_s
        born_month = (born_month.to_i - 20).to_s
      elsif born_month >= 81 && born_month <= 92 then
        born_year = "18" + born_year.to_s
        born_month = (born_month.to_i - 80).to_s
      end

      ## birth
      born_year = born_year.to_s
      born_month = born_month.to_s
      born_day = born_day.to_s
      birth = Date.parse(born_year + '-' + born_month + '-' + born_day)

      list << {
          name: row["first_name"].capitalize,
          surname: row["last_name"].capitalize,
          gender: gender,
          pesel: pesel,
          birth: birth,
          city: row["city"].capitalize
      }
    end

    begin
      Patient.create!(list)
      puts "medical_patients_importer:import success"
    rescue ActiveModel::UnknownAttributeError => invalid
      puts "medical_patients_importer:can't import: UnknownAttributeError"
    end

  end

  desc "remove medical patients data from patients table"
  task remove: :environment do
    Patient.delete_all()
    puts "medical_patients_importer:remove all data success"
  end
end
