class Visit < ApplicationRecord
  belongs_to :doctor
  belongs_to :patient
  attr_accessor :visit_date, :visit_time

  # simple validation
  validates :patient_id, presence: true
  validates :patient_id, numericality: { only_integer: true }
  validate :check_patient_exists

  validates :doctor_id, presence: true
  validates :doctor_id, numericality: { only_integer: true }
  validate :check_doctor_exists

  validates :visit_date, presence: true
  validate :check_visit_date
  validates :visit_time, presence: true
  validate :check_visit_time

  validates :visit_price, presence: true
  validate :check_visit_price

  # db validations
  validate :check_unique_visit_datetime_for_doctor

  # other
  before_validation :combine_date_and_time

  private

  def check_patient_exists
    if patient_id.present? && !Patient.exists?(patient_id)
      errors.add(:patient_id, "nie istnieje") unless errors[:patient_id].include?("nie istnieje")
    end
  end

  def check_doctor_exists
    if doctor_id.present? && !Doctor.exists?(doctor_id)
      errors.add(:doctor_id, "nie istnieje") unless errors[:doctor_id].include?("nie istnieje")
    end
  end

  def check_visit_date
    return if visit_date.blank? || visit_date.nil?
    visit_date_obj = visit_date.is_a?(String) ? Date.parse(visit_date) : visit_date

    if !(1..5).include?(visit_date_obj.wday)
      errors.add(:visit_date, "musi przypadać na dzień od poniedziałku do piątku")
    elsif visit_date_obj.present? && visit_date_obj < Date.current
      errors.add(:visit_date, 'nie może być z przeszłości')
    end
  end

  def check_visit_time
    return if visit_time.blank?
    visit_time_obj = visit_time.is_a?(String) ? Time.parse(visit_time) : visit_time

    unless (8..16).cover?(visit_time_obj.hour) && (visit_time_obj.min % 20).zero?
      errors.add(:visit_time, "musi być w zakresie od 8:00 do 16:00 i być wielokrotnością 20 minut")
    end
  rescue ArgumentError
    errors.add(:visit_time, "nieprawidłowy format czasu")
  end

  def check_visit_price
    return if visit_price.blank?

    unless visit_price.to_f >= 0
      errors.add(:visit_price, "musi być większa lub równa 0")
    end
  end

  # db validations
  def check_unique_visit_datetime_for_doctor
    return if visit_datetime.blank? || doctor_id.blank?

    existing_visits = Visit.where(visit_datetime: visit_datetime, doctor_id: doctor_id)
    existing_visits = existing_visits.where.not(id: self.id) if self.persisted?  # Ignore actual visit when editing

    if existing_visits.exists?
      errors.add(:visit_time, "jest zajęty dla danego lekarza")
    end
  end

  # other
  def combine_date_and_time
    return if visit_date.blank? || visit_time.blank?

    self.visit_datetime = DateTime.parse("#{visit_date} #{visit_time}")
  rescue ArgumentError
    errors.add(:visit_datetime, "nieprawidłowy format daty i czasu")
  end

end
