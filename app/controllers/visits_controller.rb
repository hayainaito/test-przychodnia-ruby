class VisitsController < ApplicationController
  def index
    # Patients
    @visits = Visit.includes(:doctor, :patient).all
    items = 10

    @pagy, @visits = pagy(@visits, items: items)
  end

  def new
    @visit = Visit.new(patient_id: params["patient_id"])
    @patient_name = '#' + @visit.patient_id.to_s + ' ' + Patient.find_by(id: @visit.patient_id)&.name + ' ' + Patient.find_by(id: @visit.patient_id)&.surname
    @doctors = Doctor.all
  end

  def create
    @visit = Visit.new(visit_params)
    @patient_name = '#' + @visit.patient_id.to_s + ' ' + Patient.find_by(id: @visit.patient_id)&.name + ' ' + Patient.find_by(id: @visit.patient_id)&.surname
    @doctors = Doctor.all

    if @visit.save
      render :new_completed
    else
      render :new
    end
  end

  def edit
    @visit = Visit.find(params[:id])
    @visit.visit_date = @visit.visit_datetime.strftime('%Y-%m-%d')
    @visit.visit_time = @visit.visit_datetime.strftime('%H:%M')

    @patient_name = '#' + @visit.patient_id.to_s + ' ' + Patient.find_by(id: @visit.patient_id)&.name + ' ' + Patient.find_by(id: @visit.patient_id)&.surname
    @doctors = Doctor.all

    @visit_date = @visit.visit_datetime.strftime('%Y-%m-%d')
    @visit_time = @visit.visit_datetime.strftime('%H:%M')
  end

  def update
    @visit = Visit.find(params[:id])
    @patient_name = '#' + @visit.patient_id.to_s + ' ' + Patient.find_by(id: @visit.patient_id)&.name + ' ' + Patient.find_by(id: @visit.patient_id)&.surname
    @doctors = Doctor.all  # Dostarcz listę lekarzy

    if @visit.update(visit_params)
      render :edit_completed
    else
      render :edit
    end
  end

  def destroy
    @destroy_success = true

    begin
      @visit = Visit.find(params[:id])
      @destroy_success = @visit.destroy
    rescue ActiveRecord::RecordNotFound
      @destroy_success = false
    end

    render :destroy
  end

  def available_times
    date = params[:date]
    doctor_id = params[:doctor_id]
    ignore_visit_id = params[:ignore_visit_id]

    # Check: Monday to Friday
    selected_date = Date.parse(date)
    unless (1..5).include?(selected_date.wday)
      render json: { error: 'Wybrana data nie jest dostępna' }, status: :unprocessable_entity
      return
    end

    # Check: Doctor is exists
    doctor = Doctor.find_by(id: doctor_id)
    unless doctor
      render json: { error: 'Lekarz nie istnieje' }, status: :unprocessable_entity
      return
    end

    # Get: Times
    existing_visits = Visit.where('doctor_id = ? AND visit_datetime >= ? AND visit_datetime <= ?', doctor_id, selected_date.beginning_of_day, selected_date.end_of_day)
    existing_visits = existing_visits.pluck(:visit_datetime).map { |v| v.strftime('%H:%M') }

    # Ignore specific visit
    if ignore_visit_id.present?
      ignore_visit = Visit.find_by(id: ignore_visit_id)

      if ignore_visit && ignore_visit.doctor_id.to_i == doctor_id.to_i && ignore_visit.visit_datetime.to_date == selected_date
        existing_visits -= [ignore_visit.visit_datetime.strftime('%H:%M')]
      end
    end

    all_times = ("8".."16").flat_map do |h|
      ("00".."40").step(20).flat_map do |m|
        next if h == "16" && (m == "20" || m == "40")

        h_with_zero = h.length == 1 ? "0#{h}" : h
        "#{h_with_zero}:#{m}"
      end
    end.compact

    available_times = all_times - existing_visits

    render json: { available_times: available_times }
  end


  private

  def visit_params
    params.require(:visit).permit(:patient_id, :doctor_id, :visit_price, :visit_date, :visit_time)
  end
end
