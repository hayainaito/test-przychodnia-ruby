class HomeController < ApplicationController
    def index
        @doctors_count = Doctor.count
        @patients_count = Patient.count
        @visits_count = Visit.count

        # Tworzenie zapytania SQL
        sql_query = <<-SQL
            SELECT
                strftime('%Y', birth) AS birth_year,
                COUNT(id) AS patient_count
            FROM patients
            GROUP BY 1
            ORDER BY 1
        SQL

        @patients_birth_data = Patient.find_by_sql(sql_query)
        @chart_data = {
            labels: @patients_birth_data.map { |v| v.birth_year },
            data: @patients_birth_data.map { |v| v.patient_count }
        }
    end
end
