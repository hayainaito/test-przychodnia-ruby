class PatientsController < ApplicationController
  def index
    # Pluck data
    @surnames = Patient.all
    @genders = Patient.all

    # Patients
    @patients = Patient.left_joins(:visits).group('patients.id').select('patients.*, COUNT(visits.id) as visits_count')
    items = 10

    ## sorting
    if params[:order] == 'surname'
      @patients = @patients.order(surname: :asc)
    elsif params[:order] == 'surname_desc'
      @patients = @patients.order(surname: :desc)
    elsif params[:order] == 'birth'
      @patients = @patients.order(birth: :asc)
    elsif params[:order] == 'birth_desc'
      @patients = @patients.order(birth: :desc)
    end

    # filtering
    ## surnames
    if params[:filter_surname].to_s != ''
      @patients = @patients.where("surname = ?", params[:filter_surname])
      @genders = @genders.where("surname = ?", params[:filter_surname])
    end
    ## genders
    if params[:filter_gender].to_s != ''
      @patients = @patients.where("gender = ?", params[:filter_gender])
      @surnames = @surnames.where("gender = ?", params[:filter_gender])
    end

    @surnames = @surnames.distinct.pluck(:surname)
    @genders = @genders.distinct.pluck(:gender)
    @pagy, @patients = pagy(@patients, items: items)
  end

  def visits
    patient_id = params[:patient_id]
    items = 10

    @visits = if patient_id.present?
      Visit.includes(:doctor, :patient).where(patient_id: patient_id)
    else
      []
    end
    @patient_name = '#' + patient_id.to_s + ' ' + Patient.find_by(id: patient_id)&.name + ' ' + Patient.find_by(id: patient_id)&.surname

    @pagy, @visits = pagy(@visits, items: items)
  end
end
