/**
 *= require 'jquery/dist/jquery.min'
 *= require 'rails-ujs'
 *= require 'admin-lte/plugins/jquery-ui/jquery-ui.min'
 *= require 'admin-lte/plugins/bootstrap/js/bootstrap.min'
 *= require 'admin-lte/plugins/select2/js/select2.min'
 *= require 'admin-lte/plugins/chart.js/Chart.min'
 *= require 'admin-lte/dist/js/adminlte.min'
 */

$(function () {
  // Visitors list
  if($("#patients").length > 0){
    let $form = $("#filter-form");
    const $filters = $('select[name=filter_surname], select[name=filter_gender]');
    
    // Surname adv select
    const $filter_surname = $('select[name=filter_surname]');
    $filter_surname.select2({
      placeholder: "Filtruj",
      allowClear: true,
      maximumInputLength: 100
    });

    // Gender adv select
    const $filter_gender = $('select[name=filter_gender]');
    $filter_gender.select2({
      placeholder: "Filtruj",
      allowClear: true,
      minimumResultsForSearch: Infinity
    });

    // Gender filter
    document.getElementsByName("filter_gender")[0].onchange = () => {
      document.getElementById("filter-form").submit();
    }

    $filters.on('change', () => {
      $form.submit();
    });
  }

  // Home
  if($("#home").length > 0){
    var areaChartData = {
      labels  : document.chart_data.labels,
      datasets: [{
        label: 'Ilość pacjentów',
        backgroundColor: 'rgba(60,141,188,0.9)',
        borderColor: 'rgba(60,141,188,0.8)',
        pointRadius: false,
        pointColor: '#3b8bba',
        pointStrokeColor: 'rgba(60,141,188,1)',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data: document.chart_data.data,
      }]
    }

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d');
    var barChartData = $.extend(true, {}, areaChartData);
    var temp0 = areaChartData.datasets[0];
    barChartData.datasets[0] = temp0;

    var barChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      datasetFill: true,
      scales: {
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Rok urodzenia'
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Ilość pacjentów'
          }
        }]
      },
      legend: {
        display: false
      }
    };

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    });
  }

  // New Visit
  if($("#visits_new").length > 0){
    // Doctors select
    const $visit_doctor = $("select[name='visit[doctor_id]']");
    $visit_doctor.select2({
      placeholder: "Wybierz lekarza",
      allowClear: true
    });
    $visit_doctor.change(() => {
      loadAvailableTimes($visit_date.val());
    });

    // Visit date picker
    const $visit_date = $("input[name='visit[visit_date]']");
    $visit_date.datepicker({
      dateFormat: 'yy-mm-dd',
      dayNamesMin: ['Nie.', 'Pon.', 'Wto.', 'Śro.', 'Czw.', 'Pią.', 'Sob.'],
      monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
      beforeShowDay: function (date) {
        var day = date.getDay();

        // Od poniedziałku do piątku
        return [(day > 0 && day < 6), ''];
      },
      minDate: 0, // Bez przeszłości
      prevText: '',
      nextText: '',
      onSelect: function(selectedDate) {
        loadAvailableTimes(selectedDate);
      },
    });

    loadAvailableTimes($visit_date.val());
  }
});

// New / Edit: Visit - Time selector
function loadAvailableTimes(selectedDate) {
  // validation
  if(String(selectedDate).length === 0){ return; }

  const doctorId = $("select[name='visit[doctor_id]']").first().val();

  // params
  let params = {
    date: selectedDate,
    doctor_id: doctorId
  };
  if(typeof document.ignore_visit_id !== 'undefined'){
    params["ignore_visit_id"] = document.ignore_visit_id
  }

  // Ajax
  $.ajax({
    url: '/visits/available_times',
    type: 'GET',
    data: params,
    dataType: 'json',
    success: function(result) {
      if(typeof result.error === 'undefined'){
        const data = result.available_times;
        const $time = $("select[name='visit[visit_time]']");
        const actual_value = $time.val();
        $time.empty();

        $time.append($("<option></option>")
          .text('Wybierz godzinę'));
        $.each(data, function(key, value) {
          $time.append($("<option></option>")
            .attr("value", value)
            .text(value));
        });

        if(actual_value.length == 0){
          actual_value = '';
        }
        $time.val(actual_value);
      }
    },
    error: function(error) {
      console.error(error);
    }
  });
}
