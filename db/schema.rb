# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2023_11_25_070504) do
  create_table "doctors", force: :cascade do |t|
    t.string "name", null: false
    t.string "surname", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "patients", force: :cascade do |t|
    t.string "name", null: false
    t.string "surname", null: false
    t.string "gender", limit: 1
    t.string "pesel", limit: 11
    t.string "city", limit: 30
    t.date "birth", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["birth"], name: "index_patients_on_birth"
    t.index ["gender"], name: "index_patients_on_gender"
    t.index ["surname"], name: "index_patients_on_surname"
  end

  create_table "visits", force: :cascade do |t|
    t.integer "patient_id", limit: 4, null: false
    t.integer "doctor_id", limit: 4, null: false
    t.datetime "visit_datetime", null: false
    t.integer "visit_price", limit: 4, null: false
    t.string "visit_currency", default: "pln"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
