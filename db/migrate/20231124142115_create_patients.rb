class CreatePatients < ActiveRecord::Migration[7.1]
  def change
    create_table :patients do |t|
      t.string :name, null: false
      t.string :surname, index: true, null: false
      t.string :gender, :limit => 1, index: true
      t.string :pesel, :limit => 11
      t.string :city, :limit => 30
      t.date :birth, index: true, null: false

      t.timestamps
    end
  end
end
