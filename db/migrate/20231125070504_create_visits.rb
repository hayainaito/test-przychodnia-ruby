class CreateVisits < ActiveRecord::Migration[7.1]
  def change
    create_table :visits do |t|
      t.integer :patient_id, :limit => 4, null: false
      t.integer :doctor_id, :limit => 4, null: false
      t.datetime :visit_datetime, null: false
      t.integer :visit_price, :limit => 4, null: false
      t.string :visit_currency, default: 'pln'

      t.timestamps
    end
  end
end
