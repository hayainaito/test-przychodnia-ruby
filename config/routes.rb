Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.

  # Defines the root path route ("/")
  root "home#index"
  get "/patients", to: "patients#index"
  get "/patients/visits", to: "patients#visits"
  get "/visits", to: "visits#index"

  resources :visits, only: [:new, :create, :edit, :update, :destroy] do
    collection do
      get 'available_times'
    end
  end
end
